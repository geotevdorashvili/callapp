import { TableColumn } from 'react-data-table-component';

export type Row = {
  id: number;
  name: string;
  email: string;
  gender: string;
  address: { street: string; city: string };
  phone: string;
};

export type DataState = {
  data: Row[];
  setData: (by: Row[]) => void;
  addRow: (by: Row) => void;
  updateRow: (by: Row) => void;
  deleteRows: (by: Row[]) => void;
};

export type ModalTypes = {
  showModal: boolean;
  toggleModal: (data: string) => void;
  identifier: string;
  getData: (data: Row) => void;
  initialData: Row;
};

export const columns: TableColumn<Row>[] = [
  {
    name: 'Name',
    selector: row => row.name,
  },
  {
    name: 'Email',
    selector: row => row.email,
  },
  {
    name: 'Gender',
    selector: row => row.gender,
  },
  {
    name: 'Address',
    selector: row => `${row.address.street}, ${row.address.city}`,
  },
  {
    name: 'Phone',
    selector: row => row.phone,
  },
];
