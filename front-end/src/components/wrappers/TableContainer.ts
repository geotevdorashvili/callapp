import styled from 'styled-components';

const TableContainer = styled.div`
  min-width: 800px;
`;

export default TableContainer;
