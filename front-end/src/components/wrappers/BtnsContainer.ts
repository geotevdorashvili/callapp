import styled from 'styled-components';

const BtnsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

export default BtnsContainer;
