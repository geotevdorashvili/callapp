import { useState } from 'react';
import styled from 'styled-components';
import useTableStore from '../store/useTableStore';
import { Row } from '../models/types';

const NoDataP = styled.p`
  font-size: 1.2rem;
  font-weight: bold;
  margin-top: 50px;
`;

const NoData = () => {
  const [loading, setLoading] = useState(true);
  const { data } = useTableStore((state: { data: Row[] }) => state);

  setTimeout(() => {
    if (!data.length) setLoading(false);
  }, 2000);

  if (loading) return <NoDataP>...Loading</NoDataP>;

  return <NoDataP>No records found.</NoDataP>;
};

export default NoData;
