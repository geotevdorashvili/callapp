import { useState } from 'react';
import DataTable from 'react-data-table-component';

import useTableStore from '../store/useTableStore';
import useFetch from '../hooks/useFetch';
import TableContainer from './wrappers/TableContainer';
import BtnsContainer from './wrappers/BtnsContainer';
import Button from './Button';
import ModalComp from './Modal';
import NoData from './NoData';
import { initialState } from '../utils/inputsInitialState';
import { Row, columns } from '../models/types';

function Table() {
  const [showAddModal, setShowAddModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [checkedRows, setCheckedRows] = useState<Row[]>([]);
  const [editRow, setEditRow] = useState(initialState);

  const { sendHttp } = useFetch();

  const { data, addRow, updateRow, deleteRows } = useTableStore(
    (state: {
      data: Row[];
      addRow: (by: Row) => void;
      updateRow: (by: Row) => void;
      deleteRows: (by: Row[]) => void;
    }) => state
  );

  const toggleModal = (identifier: string) =>
    identifier === 'add'
      ? setShowAddModal(prev => !prev)
      : setShowEditModal(prev => !prev);

  const handleAddClick = () => setShowAddModal(prev => !prev);

  const hanleDbClick = (row: Row, event: React.MouseEvent) => {
    setEditRow(row);
    setShowEditModal(prev => !prev);
  };

  const handleRowsChange = ({ selectedRows }: { selectedRows: Row[] }) =>
    setCheckedRows(selectedRows);

  const getAddRowData = (row: Row) => {
    addRow(row);
    sendHttp('http://localhost:3000/api/add', 'POST', row);
  };

  const getEditRowData = (row: Row) => {
    updateRow(row);
    sendHttp('http://localhost:3000/api/update', 'PUT', row);
  };

  const handleDeleteClick = () => {
    if (!checkedRows.length) return;

    deleteRows(checkedRows);
    sendHttp('http://localhost:3000/api/delete', 'PUT', checkedRows);
  };

  return (
    <TableContainer>
      <ModalComp
        showModal={showAddModal}
        toggleModal={toggleModal}
        identifier={'add'}
        getData={getAddRowData}
        initialData={initialState}
      />
      <ModalComp
        showModal={showEditModal}
        toggleModal={toggleModal}
        identifier={'edit'}
        getData={getEditRowData}
        initialData={editRow}
      />
      <BtnsContainer>
        <Button onClick={handleDeleteClick} color="#ff4560">
          Delete
        </Button>
        <Button onClick={handleAddClick} color="#008ffb">
          Add
        </Button>
      </BtnsContainer>
      <DataTable
        columns={columns}
        data={data}
        striped
        highlightOnHover
        pointerOnHover
        dense
        pagination
        paginationPerPage={20}
        selectableRows
        selectableRowsHighlight={true}
        noDataComponent={<NoData />}
        onRowDoubleClicked={hanleDbClick}
        onSelectedRowsChange={handleRowsChange}
      />
    </TableContainer>
  );
}

export default Table;
