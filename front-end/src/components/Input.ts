import styled from 'styled-components';

const Input = styled.input`
  width: 90%;
  height: 2rem;
  background-color: #e4e8eb;
  padding-left: 10px;
  border-radius: 3px;
  border: none;
  outline: none;

  &::placeholder {
    color: black;
  }

  &:focus {
    background-color: #454a4f;
    color: white;
  }

  &:focus::placeholder {
    color: white;
  }
`;

export default Input;
