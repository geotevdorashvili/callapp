import Input from './Input';
import Select from './Select';
import { Row } from '../models/types';

const Inputs: React.FC<{
  userInputs: Row;
  handleChange: Function;
}> = ({ userInputs, handleChange }) => {
  return (
    <>
      <Input
        placeholder="Name"
        value={userInputs.name}
        onChange={handleChange.bind(this, 'name')}
      />
      <Input
        placeholder="Email"
        value={userInputs.email}
        onChange={handleChange.bind(this, 'email')}
      />
      <Select defaultValue="default" onChange={handleChange.bind(this, 'gender')}>
        <option value="default" disabled hidden>
          {userInputs.gender ? userInputs.gender : 'Gender'}
        </option>

        <option value="female">female</option>
        <option value="male">male</option>
      </Select>
      <Input
        placeholder="Street"
        value={userInputs.address.street}
        onChange={handleChange.bind(this, 'street')}
      />
      <Input
        placeholder="City"
        value={userInputs.address.city}
        onChange={handleChange.bind(this, 'city')}
      />
      <Input
        placeholder="Phone"
        value={userInputs.phone}
        onChange={handleChange.bind(this, 'phone')}
      />
    </>
  );
};

export default Inputs;
