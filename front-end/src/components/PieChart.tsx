import Chart from 'react-apexcharts';
import useTableStore from '../store/useTableStore';

const PieChart = () => {
  const { data } = useTableStore(state => state);

  const cities = data.map(el => el.address.city);

  const obj: { [key: string]: number } = {};

  cities.forEach(city => {
    if (!obj[city]) obj[city] = 0;
    obj[city]++;
  });

  const options = { labels: Object.keys(obj) };

  const series = Object.values(obj);

  return (
    <div>
      <Chart options={options} series={series} type="pie" width="400" />
    </div>
  );
};
export default PieChart;
