import styled from 'styled-components';

const Select = styled.select`
  width: 90%;
  height: 2rem;
  border-radius: 3px;
  background-color: #e4e8eb;
  border: none;
  outline: none;
  padding-left: 10px;
  background-image: url(http://localhost:5173/src/assets/dropdown.png);
  background-repeat: no-repeat;
  background-position: 95% 55%;
  background-size: 5%;
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;

  option {
    background-color: #454a4f;
    color: white;
  }
`;

export default Select;
