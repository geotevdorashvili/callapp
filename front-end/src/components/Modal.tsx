import { useEffect, useState } from 'react';
import { Modal, ModalBody, ModalFooter } from 'reactstrap';
import styled from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';

import BtnsContainer from './wrappers/BtnsContainer';
import Button from './Button';
import Form from './Form';
import { ModalTypes } from '../models/types';
import { initialState } from '../utils/inputsInitialState';
import Inputs from './Inputs';

const ErrorP = styled.p`
  position: absolute;
  bottom: 10px;
  color: #ff4560;
  margin: 0;
`;

const ModalComp: React.FC<ModalTypes> = ({
  showModal,
  toggleModal,
  identifier,
  getData,
  initialData,
}) => {
  const [userInputs, setUserInputs] = useState(initialData);
  const [error, setError] = useState(false);
  const [phoneError, setPhoneError] = useState(false);

  useEffect(() => {
    setUserInputs(initialData);
  }, [initialData]);

  const handleToggle = (id: string) => {
    if (id === 'add') setUserInputs(initialState);

    setError(false);
    setPhoneError(false);
    toggleModal(id);
  };

  const handleChange: Function = (
    inputName: string,
    e: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    setError(false);
    setPhoneError(false);

    if (inputName === 'street') {
      return setUserInputs(prevState => ({
        ...prevState,
        address: { ...prevState.address, street: e.target.value },
      }));
    }
    if (inputName === 'city') {
      return setUserInputs(prevState => ({
        ...prevState,
        address: { ...prevState.address, city: e.target.value },
      }));
    }
    setUserInputs(prevState => ({ ...prevState, [inputName]: e.target.value }));
  };

  const handleSubmit = () => {
    if (Object.values(userInputs).some(el => el === '')) return setError(true);
    if (!/^[+\-()0-9 ]*$/.test(userInputs.phone)) return setPhoneError(true);

    handleToggle(identifier);
    getData(userInputs);
    setUserInputs(initialState);
  };

  return (
    <Modal
      isOpen={showModal}
      toggle={() => handleToggle(identifier)}
      centered={true}
      size="sm"
    >
      <ModalBody>
        <Form>
          <Inputs userInputs={userInputs} handleChange={handleChange} />

          {error && <ErrorP>Please fill out all inputs</ErrorP>}
          {phoneError && <ErrorP>Invalid phone number format</ErrorP>}
        </Form>
      </ModalBody>

      <ModalFooter>
        <BtnsContainer>
          <Button onClick={handleSubmit} color="#008ffb">
            {identifier === 'add' ? 'Add' : 'Edit'}
          </Button>
          <Button onClick={() => handleToggle(identifier)} color="#ff4560">
            Cancel
          </Button>
        </BtnsContainer>
      </ModalFooter>
    </Modal>
  );
};

export default ModalComp;
