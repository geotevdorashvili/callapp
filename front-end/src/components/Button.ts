import styled from 'styled-components';

const Button = styled.button`
  border: none;
  padding: 2px 10px;
  border-radius: 5px;
  color: white;
  background-color: ${({ color }) => color};

  &:hover {
    opacity: 0.7;
  }

  &:active {
    opacity: 0.5;
  }
`;

export default Button;
