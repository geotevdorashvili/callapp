export const initialState = {
  id: 1,
  name: '',
  email: '',
  gender: '',
  address: { street: '', city: '' },
  phone: '',
};
