import create from 'zustand';
import { v4 as uuidv4 } from 'uuid';
import { DataState } from '../models/types';

const useTableStore = create<DataState>()(set => ({
  data: [],
  setData: by => set(() => ({ data: [...by] })),
  addRow: by =>
    set(state => {
      const updatedRow = { ...by, id: parseInt(uuidv4(), 16) };

      return { data: [...state.data, updatedRow] };
    }),
  updateRow: by =>
    set(state => {
      const updatedData = state.data.map(el => (el.id === by.id ? by : el));

      return { data: [...updatedData] };
    }),
  deleteRows: by =>
    set(state => {
      const ids = by.map(el => el.id);
      const updatedData = state.data.filter(el => !ids.includes(el.id));

      return { data: [...updatedData] };
    }),
}));

export default useTableStore;
