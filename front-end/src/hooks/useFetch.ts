import { useState } from 'react';
import { Row } from '../models/types';

const useFetch = () => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const sendHttp = async (url: string, method: string, body: Row | Row[] | {}) => {
    try {
      const response =
        Object.keys(body).length > 0
          ? await fetch(url, {
              method,
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(body),
            })
          : await fetch(url, {});

      const data = await response.json();

      setResponse(data);
      setIsLoading(false);
    } catch (error: any) {
      setError(error);
      setIsLoading(false);
    }
  };

  return { response, error, isLoading, sendHttp };
};

export default useFetch;
