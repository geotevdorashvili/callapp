import { useEffect } from 'react';
import PieChart from './components/PieChart';

import Table from './components/Table';
import useTableStore from './store/useTableStore';
import useFetch from './hooks/useFetch';
import AppContainer from './components/wrappers/AppContainer';
import { Row } from './models/types';

function App() {
  const { response, sendHttp } = useFetch();
  const { setData } = useTableStore(
    (state: { data: Row[]; setData: (by: Row[]) => void }) => state
  );

  useEffect(() => {
    sendHttp('http://localhost:3000/api', 'GET', {});
  }, []);

  useEffect(() => {
    if (response) setData(response);
  }, [response]);

  return (
    <AppContainer>
      <Table />
      <PieChart />
    </AppContainer>
  );
}

export default App;
