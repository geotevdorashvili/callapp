const fs = require('fs');
const Router = require('koa-router');
const data = require('./data.json');
const { v4: uuidv4 } = require('uuid');

const router = new Router({
  prefix: '/api',
});

router.get('/', (ctx, next) => {
  ctx.body = data;
  next();
});

router.post('/add', (ctx, next) => {
  fs.readFile('data.json', 'utf-8', (err, jsonData) => {
    if (err) throw err;

    const data = JSON.parse(jsonData);

    const updatedRow = { ...ctx.request.body, id: parseInt(uuidv4(), 16) };

    data.push(updatedRow);

    fs.writeFile('data.json', JSON.stringify(data), err => {
      if (err) throw err;
    });
  });

  ctx.response.status = 201;
  ctx.body = `New item added `;

  next();
});

router.put('/update', (ctx, next) => {
  fs.readFile('data.json', 'utf-8', (err, jsonData) => {
    if (err) throw err;

    const data = JSON.parse(jsonData);

    const updatedData = data.map(el =>
      el.id === ctx.request.body.id ? ctx.request.body : el
    );

    fs.writeFile('data.json', JSON.stringify(updatedData), err => {
      if (err) throw err;
    });
  });

  ctx.response.status = 201;
  ctx.body = `Item got updated `;

  next();
});

router.put('/delete', (ctx, next) => {
  fs.readFile('data.json', 'utf-8', (err, jsonData) => {
    if (err) throw err;
    const body = ctx.request.body;
    const data = JSON.parse(jsonData);

    const ids = body.map(el => el.id);
    const updatedData = data.filter(el => !ids.includes(el.id));

    fs.writeFile('data.json', JSON.stringify(updatedData), err => {
      if (err) throw err;
    });
  });

  ctx.response.status = 201;
  ctx.body = `item removed `;

  next();
});

module.exports = router;
