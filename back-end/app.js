const Koa = require('koa');
const { koaBody } = require('koa-body');
const cors = require('@koa/cors');
const api = require('./api.js');

const app = new Koa();

app.use(cors());

app.use(koaBody());

app.use(api.routes());

app.listen(3000);
